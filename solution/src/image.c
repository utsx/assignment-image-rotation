//
// Created by dimma on 13.12.2021.
//
#include "image.h"
#include <inttypes.h>
#include <malloc.h>
#include <stdlib.h>


struct image create_image(uint64_t width, uint64_t height) {
    struct image image;
    image.width = width;
    image.height = height;
    image.data = malloc(sizeof(struct pixel) * width * height);
    return image;
}

struct pixel * get_pixel(struct image const* image, uint64_t x, uint64_t y)
{
    return &image->data[x + y * image->width];
}
void destroy_image(struct image* image){
    free(image->data);
}








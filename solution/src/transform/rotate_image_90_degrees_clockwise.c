//
// Created by dimma on 28.12.2021.
//
#include "image.h"

struct image rotate_image_90_degrees_clockwise(struct image const source, struct image output){
    uint64_t i = 0;
    for(size_t x = output.width; x > 0; x--)
    {
        for(size_t y = 0; y < output.height; y++)
        {
            //output -> data[output -> width * x + source -> height - 1 - y] = source -> data[source -> width * y + x];
            struct pixel* pixel = get_pixel(&output, x-1, y);
            *pixel = source.data[i];
            i++;
        }
    }
    return output;
}

//
// Created by dimma on 13.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>


struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

#pragma pack(push, 1)
struct pixel {
    uint8_t r, g, b;
};
#pragma pack(pop)
struct image create_image(uint64_t width, uint64_t height);

void destroy_image(struct image* image);

struct pixel * get_pixel(struct image const* image, uint64_t x, uint64_t y);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H



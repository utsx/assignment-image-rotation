//
// Created by dimma on 24.01.2022.
//
#include "image.h"
#include "transform/rotate_image_90_degrees_clockwise.h"
#include "transform/transform.h"
struct image transform(struct image const image, struct image (image_transform) (struct image const, struct image))
{
    uint64_t height = image.height;
    uint64_t width = image.width;
    struct image output = create_image(height, width);
    return image_transform(image, output);
}

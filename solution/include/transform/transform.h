//
// Created by dimma on 24.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H

struct image transform(struct image const image, struct image (image_transform) (struct image const, struct image));

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H

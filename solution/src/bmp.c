//
// Created by dimma on 13.12.2021.
//
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"

static uint64_t const TYPE = 19778;
static uint64_t const RESERVED = 0;
static uint64_t const HEADER_SIZE = 40;
static uint64_t const PLANES = 1;
static uint64_t const COMPRESSION = 0;
static uint64_t const PIXEL_PER_M = 0;
static uint64_t const COLORS_USED = 0;
static uint64_t const COLORS_IMPORTANT = 0;
static size_t const BIT_COUNT = 24;


#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static const char * bmp_read_error_msg[] = {
        [READ_OK] = "Read OK",
        [READ_FAILED] = "Read failed",
        [READ_INVALID_BITS] = "Read invalid bits",
        [READ_INVALID_HEADER] = "Read invalid header",
        [READ_STREAM_NULL] = "Read stream null",
        [INPUT_FILE_NULL] =
        "Input file null"};

static const char * bmp_write_error_msg[] = {
        [WRITE_OK] = "Write OK",
        [WRITE_INVALID_SOURCE] = "Write invalid source",
        [WRITE_HEADER_ERROR] = "Write header error",
        [WRITE_ERROR] = "Write error",
        [WRITE_STREAM_NULL] = "Write stream null"};


static struct bmp_header header_init(struct image const* image, size_t padding);
static size_t calculation_padding(size_t width);
static void check_padding(size_t padding, struct pixel pixel, FILE* out);
static enum read_status check_header(struct bmp_header header);

const char* get_write_error_msg(enum write_status status){
    return bmp_write_error_msg[status];
}
const char* get_read_error_msg(enum read_status status){
    return bmp_read_error_msg[status];
}


struct image image_from_bmp(const char* file_name)
{
    FILE* input_file;
    bool open_status = open_file(&input_file, file_name, "rb");
    if(open_status == false)
    {
        fprintf(stderr, "File open failed");
    }
    else
    {
        fprintf(stdout, "File opened");
    }
    struct image input_image = {0};
    enum read_status read_status = from_bmp(input_file, &input_image);
    if(read_status != 0)
    {
        fprintf(stderr, "%s\n", get_read_error_msg(read_status));
    }
    else
    {
        fprintf(stdout, "Read OK");
    }
    if(close_file(input_file) == 1)
    {
        fprintf(stderr, "File close failed");
    }
    else
    {
        fprintf(stdout, "File closed");
    }
    return input_image;
}

void image_to_bmp(struct image output_image, const char* file_name)
{
    FILE* output_file;
    bool open_status = open_file(&output_file, file_name, "wb");
    if(open_status == false)
    {
        fprintf(stderr, "File open failed");
    }
    else
    {
        fprintf(stdout, "File opened");
    }
    enum write_status write_status = to_bmp(output_file, &output_image);
    if(write_status != 0)
    {
        fprintf(stderr, "%s\n", get_write_error_msg(write_status));
    }
    else
    {
        fprintf(stdout, "Write OK");
    }
    if( close_file(output_file) == 1)
    {
        fprintf(stderr, "File close failed");
    }
    else
    {
        fprintf(stdout, "File closed");
    }
}



enum read_status from_bmp(FILE* in, struct image *image ){
    if(!in)
    {
        return READ_STREAM_NULL;
    }
    if(!image)
    {
        return INPUT_FILE_NULL;
    }
    struct bmp_header header;
    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1)
    {
        return READ_FAILED;
    }
    if(check_header(header) != READ_OK)
    {
        return check_header(header);
    }
    //d
    *image = create_image(header.biWidth, header.biHeight);
    size_t padding = calculation_padding(header.biWidth);
    for (size_t i = 0; i < header.biHeight; i++){
        if(fread(image -> data + i * (image -> width), sizeof(struct pixel) * (size_t) (image -> width), 1, in) != 1){
            return READ_FAILED;
        }
        if(fseek(in, padding, SEEK_CUR) != 0){
            return READ_FAILED;
        }
    }
    return READ_OK;
}

static struct bmp_header header_init(struct image const* image, size_t padding) {
    return (struct bmp_header) {
            .bfType = TYPE,
            .biSize = HEADER_SIZE,
            .bfReserved = RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = COMPRESSION,
            .biSizeImage = ((image->width + padding) * image->height) * sizeof(struct pixel),
            .bfileSize = ((image->width + padding) * image->height) * sizeof(struct pixel) + sizeof(struct bmp_header),
            .biXPelsPerMeter = PIXEL_PER_M,
            .biYPelsPerMeter = PIXEL_PER_M,
            .biClrUsed = COLORS_USED,
            .biClrImportant = COLORS_IMPORTANT,
    };
}

enum write_status to_bmp(FILE* out, struct image const *image ){

    const uint64_t height = image->height;
    const uint64_t width = image->width;
    size_t padding = calculation_padding(width);
    struct bmp_header header = header_init(image, padding);
    if(fwrite(&header, sizeof(struct bmp_header), 1, out) != 1)
    {
        return WRITE_ERROR;
    }
    if(fseek(out, header.bOffBits, SEEK_SET) != 0)
    {
        return WRITE_ERROR;
    }
    struct pixel pixel = {0};
    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            pixel = image->data[width * i + j];
            if(fwrite(&pixel, sizeof(struct pixel), 1, out) != 1)
            {
                return WRITE_ERROR;
            }
        }
        check_padding(padding, pixel, out);
    }
    return WRITE_OK;
}

static size_t calculation_padding(size_t width) {
    return (4 - width * sizeof(struct pixel) % 4) % 4;
}

static void check_padding(size_t padding, struct pixel pixel, FILE* out)
{
    if (padding != 0)
    {
        for (size_t j = 0; j < padding; j++) {
            pixel.r = 0;
            pixel.g = 0;
            pixel.b = 0;
            if(fwrite(&pixel, sizeof(unsigned char), 1, out) != 1)
            {
                fprintf(stderr, "%s\n", bmp_write_error_msg[WRITE_ERROR]);
            }
        }
    }
}
static enum read_status check_header(struct bmp_header header)
{
    if (header.biBitCount != BIT_COUNT)
    {
        return READ_INVALID_BITS;
    }
    if (header.bOffBits != sizeof(struct bmp_header))
    {
        return READ_INVALID_HEADER;
    }
    if (header.biSize != HEADER_SIZE)
    {
        return READ_INVALID_HEADER;
    }
    if (header.biPlanes != PLANES)
    {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}


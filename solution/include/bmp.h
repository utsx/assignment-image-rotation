//
// Created by dimma on 13.12.2021.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "file.h"
#include "image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>


enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_SOURCE,
    WRITE_HEADER_ERROR,
    WRITE_ERROR,
    WRITE_STREAM_NULL
};

enum read_status {
    READ_OK = 0,
    READ_FAILED,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_STREAM_NULL,
    INPUT_FILE_NULL
};

struct image image_from_bmp(const char* file_name);
void image_to_bmp(struct image output_image, const char* file_name);

enum read_status from_bmp(FILE* in, struct image* image);

enum write_status to_bmp(FILE* out, struct image const* image);

const char* get_write_error_msg(enum write_status status);
const char* get_read_error_msg(enum read_status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H

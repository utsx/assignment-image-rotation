//
// Created by dimma on 28.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "image.h"

struct image rotate_image_90_degrees_clockwise(struct image const source, struct image output);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

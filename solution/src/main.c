#include "bmp.h"
#include "transform/rotate_image_90_degrees_clockwise.h"
#include "transform/transform.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {

    if(argc != 3)
    {
        fprintf(stderr, "%s\n", "Usage: ./image-transformer <source-image> <transformed-image>");
        return 1;
    }
    struct image input_image = image_from_bmp(argv[1]);

    if(input_image.data == NULL){
        fprintf(stderr, "%s\n", "Bad input data");
        return 1;
    }

    struct image output_image = transform(input_image, rotate_image_90_degrees_clockwise);

    if(output_image.data == NULL){
        fprintf(stderr, "%s\n", "Bad transformation");
        return 1;
    }

    destroy_image(&input_image);
    image_to_bmp(output_image, argv[2]);
    destroy_image(&output_image);
    return 0;
}

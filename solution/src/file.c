//
// Created by dimma on 23.12.2021.
//

#include "file.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool open_file(FILE** file, const char* name, const char* mode) {
    *file = fopen(name, mode);
    if(!file)
    {
        return false;
    }
    return true;
}

int close_file(FILE* file) {
    return fclose(file);
}

